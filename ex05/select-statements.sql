-- 1. All information about all the members of the video store.
SELECT * FROM videoMembers;

-- 2. Everything about all the members of the video store, except the number of video hires they have.
SELECT fullname, gender, dateofbirth, joined FROM videoMembers;

-- 3. All the titles to the articles that have been written.
SELECT title FROM articles;

-- 4. All the directors of the movies the video store has (without repeating any names).
SELECT DISTINCT(director) FROM rentalMovies;

-- 5. All the video titles that rent for $2 or less per week.
SELECT title FROM rentalMovies WHERE charge <= 2;

-- 6. A sorted list of all the usernames that have been registered.
SELECT username FROM registeredUsers ORDER BY username; -- ascending
SELECT username FROM registeredUsers ORDER BY username DESC; -- descending

-- 7. All the usernames where the user’s first name starts with the letters ‘Pete’.
SELECT username FROM registeredUsers WHERE fname LIKE 'Pete%';

-- 8. All the usernames where the user’s first name or last name starts with the letters ‘Pete’.
SELECT username FROM registeredUsers WHERE fname LIKE 'Pete%' OR lname LIKE 'Pete%';
