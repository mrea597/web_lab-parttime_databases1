drop table if exists registeredUsers;

create table registeredUsers (
    fname varchar(20),
    lname varchar(90),
    username varchar(20),
    email varchar(20)
);

insert into registeredUsers values ('Peter', 'Potato', 'ppota1', 'ppotato@mail.org');
insert into registeredUsers values ('Pete', 'Washington', 'pwash2', 'pwash@mail.ru');
insert into registeredUsers values ('Lana', 'Peterson', 'lpete3', 'lanapete@gmail.com');
insert into registeredUsers values ('Teddy', 'Bear', 'tbear4', 'teddy@bear.com');


