drop table if exists articles;
drop table if exists comments;

create table articles (
    articleid INT NOT NULL,
    title varchar(100),
    content text
);

create table comments (
    commentId INTEGER PRIMARY KEY AUTOINCREMENT,
    articleId INT NOT NULL,
    commentText TEXT,
    FOREIGN KEY (articleId) REFERENCES articles (articleId)
);

insert into articles values (
    1,
    "World War II",
    "World War II (often abbreviated to WWII or WW2), also known as the Second World War, was a global war that lasted from 1939 to 1945. The vast majority of the world's countries—including all the great powers—eventually formed two opposing military alliances: the Allies and the Axis. A state of total war emerged, directly involving more than 100 million people from over 30 countries. The major participants threw their entire economic, industrial, and scientific capabilities behind the war effort, blurring the distinction between civilian and military resources. World War II was the deadliest conflict in human history, marked by 50 to 85 million fatalities, most of whom were civilians in the Soviet Union and China."
);

insert into articles values (
    2,
    "Battle of Midway",
    "The Battle of Midway was a decisive naval battle in the Pacific Theater of World War II which occurred between 4 and 7 June 1942, only six months after Japan's attack on Pearl Harbor and one month after the Battle of the Coral Sea. The United States Navy under Admirals Chester Nimitz, Frank Jack Fletcher, and Raymond A. Spruance defeated an attacking fleet of the Imperial Japanese Navy under Admirals Isoroku Yamamoto, Chūichi Nagumo, and Nobutake Kondō near Midway Atoll, inflicting devastating damage on the Japanese fleet that proved irreparable. Military historian John Keegan called it 'the most stunning and decisive blow in the history of naval warfare'"
);

insert into articles values (
    3,
    "Battle of the Coral Sea",
    "The Battle of the Coral Sea, fought from 4 to 8 May 1942, was a major naval battle between the Imperial Japanese Navy (IJN) and naval and air forces from the United States and Australia, taking place in the Pacific Theatre of World War II. The battle is historically significant as the first action in which aircraft carriers engaged each other, as well as the first in which neither side's ships sighted or fired directly upon the other."
);

insert into comments (articleId, commentText) values (1, 'first!');
insert into comments (articleId, commentText) values (1, 'wow, I had no idea this happened!');
insert into comments (articleId, commentText) values (2, 'I was midway through this article and I decided to comment');
insert into comments (articleId, commentText) values (2, 'I disagree with this article and will be submitting a written complaint to your supervisor');
insert into comments (articleId, commentText) values (3, 'cool story bro');
insert into comments (articleId, commentText) values (3, 'I have learnt so much from this article, thank you!');
insert into comments (articleId, commentText) values (3, 'has anyone seen my cat???');

