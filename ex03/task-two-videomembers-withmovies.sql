drop table if exists videoMembers;
drop table if exists rentalMovies;

create table videoMembers (
    fullname varchar(50),
    gender varchar(6),
    dateofbirth INT NOT NULL,
    joined char(4),
    numhires INT NOT NULL
);

create table rentalMovies (
    barcode INT NOT NULL,
    title varchar(100),
    director varchar(50),
    charge INT NOT NULL,
    hiredBy varchar(50),
    PRIMARY KEY (barcode),
    FOREIGN KEY (hiredBy) REFERENCES videoMembers (fullname)
);

insert into videoMembers values ('Peter Jackson', 'male', 1961, '1997', 17000);
insert into videoMembers values ('Jane Campion', 'female', 1954, '1980', 30000);
insert into videoMembers values ('Roger Donaldson', 'male', 1945, '1980', 12000);
insert into videoMembers values ('Temuera Morrison', 'male', 1960, '1995', 15500);
insert into videoMembers values ('Russell Crowe', 'male', 1964, '1990', 10000);
insert into videoMembers values ('Lucy Lawless', 'female', 1968, '1995', 5000);
insert into videoMembers values ('Michael Hurst', 'male', 1957, '2000', 15000);
insert into videoMembers values ('Andrew Niccol', 'male', 1964, '1997', 3500);
insert into videoMembers values ('Kiri Te Kanawa', 'female', 1944, '1997', 500);		
insert into videoMembers values ('Lorde', 'female', 1996, '2010', 1000);	
insert into videoMembers values ('Scribe', 'male', 1979, '2000', 5000);	
insert into videoMembers values ('Kimbra', 'female', 1990, '2005', 7000);
insert into videoMembers values ('Neil Finn', 'male', 1958, '1985', 6000);
insert into videoMembers values ('Anika Moa', 'female', 1980, '2000', 700);
insert into videoMembers values ('Bic Runga', 'female', 1976, '1995', 5000);
insert into videoMembers values ('Ernest Rutherford', 'male', 1871, '1930', 4200);
insert into videoMembers values ('Kate Sheppard', 'female', 1847, '1930', 1000);
insert into videoMembers values ('Apirana Turupa Ngata', 'male', 1874, '1920', 3500);
insert into videoMembers values ('Edmund Hillary', 'male', 1919, '1955', 10000);
insert into videoMembers values ('Katherine Mansfield', 'female', 1888, '1920', 2000);
insert into videoMembers values ('Margaret Mahy', 'female', 1936, '1985', 5000);
insert into videoMembers values ('John Key', 'male', 1961, '1990', 20000);
insert into videoMembers values ('Sonny Bill Williams', 'male', 1985, '1995', 15000);
insert into videoMembers values ('Dan Carter', 'male', 1982, '1990', 20000);	
insert into videoMembers values ('Bernice Mene', 'female', 1975, '1990', 30000);		

insert into rentalMovies values (4623, 'Star Wars: Episode I – The Phantom Menace', 'George Lucas', 2, null);
insert into rentalMovies values (4624, 'Star Wars: Episode II – Attack of the Clones', 'George Lucas', 2, 'Scribe');
insert into rentalMovies values (4625, 'Star Wars: Episode III – Revenge of the Sith', 'George Lucas', 4, 'Russell Crowe');
insert into rentalMovies values (1145, 'The Matrix', 'The Wachowskis', 6, 'Dan Carter');
insert into rentalMovies values (2277, 'Notting Hill', 'Roger Michell', 2, 'John Key');
insert into rentalMovies values (8899, 'Austin Powers: The Spy Who Shagged Me', 'Jay Roach', 4, 'Anika Moa');
insert into rentalMovies values (9090, 'Tarzan', 'Chris Buck & Kevin Lima', 6, 'Edmund Hillary');