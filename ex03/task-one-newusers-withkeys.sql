drop table if exists registeredUsers;

create table registeredUsers (
    fname varchar(20),
    lname varchar(90),
    username varchar(20),
    email varchar(20),
    PRIMARY KEY (username)
);

insert into registeredUsers values ('Peter', 'Potato', 'ppota1', 'ppotato@mail.org');
insert into registeredUsers values ('Pete', 'Washington', 'pwash2', 'pwash@mail.ru');
insert into registeredUsers values ('Lana', 'Peterson', 'lpete3', 'lanapete@gmail.com');
insert into registeredUsers values ('Teddy', 'Bear', 'tbear4', 'teddy@bear.com');

-- the below will result in the following error due to duplicate username:
-- Error: near line 17: UNIQUE constraint failed: registeredUsers.username
-- insert into registeredUsers values ('Teddy', 'Bear Jr', 'tbear4', 'teddy@bear.com');

