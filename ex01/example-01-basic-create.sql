drop table if exists students; -- Delete the students table if it's already there...

create table students ( -- Create a new table, called "students".
    id int not null, -- id is an integer and can't be null.
    fname varchar(20), -- fname is a string with a max. of 20 characters.
    lname varchar(100), -- lname is a string with a max. of 100 characters.
    country char(2), -- country is a string with exactly 2 characters. If less than 2 characters are supplied, the remainder will be whitespace.
    primary key (id) -- The id field is the key. There can only be one of each id (i.e. ids are unique).
);

insert into students values (1234, 'teddy', 'bear', 'CN');
insert into students values (2134, 'talon', 'fredericks', 'US');
insert into students values (8888, 'shooter', 'mcgregor', 'AU');
insert into students values (9090, 'potato', 'nuggets', 'NZ');
